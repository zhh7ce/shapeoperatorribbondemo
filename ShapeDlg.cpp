// ShapeDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "RibbonControls.h"
#include "ShapeDlg.h"
#include "afxdialogex.h"

#define PI 3.1415927

IMPLEMENT_DYNAMIC(CShapeDlg, CDialogEx)

CShapeDlg::CShapeDlg(CWnd* pParent /*=NULL*/) {
	m_o = NULL;
	m_pDoc = NULL;
}

CShapeDlg::~CShapeDlg() {
	OnCancel();
}

void CShapeDlg::OnCancel() {
	if (NULL != m_o) m_o->m_visible = false;
	DestroyWindow();
}
void CShapeDlg::PostNcDestroy() {
	CDialog::PostNcDestroy();
	delete this;
}

void CShapeDlg::LoadPtr(COperator* o, CDemoDoc* pDoc) {
	this->m_o = o;
	this->m_pDoc = pDoc;
}

// CCubeDlg 对话框

IMPLEMENT_DYNAMIC(CCubeDlg, CDialogEx)

CCubeDlg::CCubeDlg(CWnd* pParent /*=NULL*/){}

CCubeDlg::~CCubeDlg()
{
}

BEGIN_MESSAGE_MAP(CCubeDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CCubeDlg::OnBnClickedOk)
END_MESSAGE_MAP()

// CCubeDlg 消息处理程序
void CCubeDlg::OnBnClickedOk() {
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();
	int a = GetDlgItemInt(IDC_CUBE_EDITX, NULL, TRUE);
	int b = GetDlgItemInt(IDC_CUBE_EDITY, NULL, TRUE);
	int c = GetDlgItemInt(IDC_CUBE_EDITZ, NULL, TRUE);
	GLfloat x = a;
	GLfloat y = b;
	GLfloat z = c;
	glm::vec4 temp_vec[8];
	temp_vec[0] = glm::vec4(0, 0, 0, 1);
	temp_vec[1] = glm::vec4(0, y, 0, 1);
	temp_vec[2] = glm::vec4(x, 0, 0, 1);
	temp_vec[3] = glm::vec4(x, y, 0, 1);
	temp_vec[4] = glm::vec4(0, 0, z, 1);
	temp_vec[5] = glm::vec4(0, y, z, 1);
	temp_vec[6] = glm::vec4(x, 0, z, 1);
	temp_vec[7] = glm::vec4(x, y, z, 1);

	if (NULL != m_o) {
		m_o->m_object->LoadPoints(temp_vec);
	} else {
		CShape* c = new CCube();
		c->LoadPoints(temp_vec);

		COperator* o = new COperator();
		o->m_object = c;
		o->m_object_type = 1;
		m_pDoc->m_operator_list.AddTail(o);
		m_pDoc->m_shape_tree->InsertItem(_T("Cube"), m_pDoc->m_tree_root);
	}
}

// CCylinderDlg 对话框

IMPLEMENT_DYNAMIC(CCylinderDlg, CDialogEx)

CCylinderDlg::CCylinderDlg(CWnd* pParent /*=NULL*/){}

CCylinderDlg::~CCylinderDlg(){}

BEGIN_MESSAGE_MAP(CCylinderDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CCylinderDlg::OnBnClickedOk)
END_MESSAGE_MAP()

void CCylinderDlg::OnBnClickedOk() {
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();
	glm::vec4 temp_vec[128];

	int a = GetDlgItemInt(IDC_CYL_EDITR, NULL, TRUE);
	int b = GetDlgItemInt(IDC_CYL_EDITH, NULL, TRUE);
	GLfloat radius = a;
	GLfloat height = b;

	GLfloat x = 0.0;
	GLfloat y = 0.0;
	GLfloat angle = 0.0;
	GLfloat angle_stepsize = 0.1;

	/** Draw the tube */
	angle = 0.0;
	int cnt = 0;
	while (cnt < 126) {
		x = radius * cos(angle);
		y = radius * sin(angle);
		temp_vec[cnt] = glm::vec4(x, y, height, 1.0f);
		cnt++;
		temp_vec[cnt] = glm::vec4(x, y, 0.0, 1.0f);
		cnt++;
		angle = angle + angle_stepsize;
	}
	temp_vec[cnt] = glm::vec4(radius, 0.0, height, 1.0f);
	cnt++;
	temp_vec[cnt] = glm::vec4(radius, 0.0, 0.0, 1.0f);
	cnt++;
	if (NULL != m_o) {
		m_o->m_object->LoadPoints(temp_vec);
	} else {
		CShape* c = new CCylinder();
		c->LoadPoints(temp_vec);

		COperator* o = new COperator();
		o->m_object = c;
		o->m_object_type = 2;
		m_pDoc->m_operator_list.AddTail(o);
		m_pDoc->m_shape_tree->InsertItem(_T("Cylinder"), m_pDoc->m_tree_root);
	}
}


// CoperatorDlg

IMPLEMENT_DYNAMIC(COperatorDlg, CDialogEx)

COperatorDlg::COperatorDlg(CWnd* pParent /*=NULL*/){}

COperatorDlg::~COperatorDlg() {}

BEGIN_MESSAGE_MAP(COperatorDlg, CShapeDlg)
	ON_BN_CLICKED(IDC_RADIO1, &COperatorDlg::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &COperatorDlg::OnBnClickedRadio2)
END_MESSAGE_MAP()


void COperatorDlg::OnBnClickedRadio1() {
	// TODO: 
	//pRadio1->SetCheck(true);
	m_o->m_move_shape = true;
	m_o->m_focused = -1;
}


void COperatorDlg::OnBnClickedRadio2() {
	// TODO: 在此添加控件通知处理程序代码
	//pRadio2->SetCheck(true);
	m_o->m_move_shape = false;
	m_o->m_focused = 0;
}


BOOL COperatorDlg::OnInitDialog() {
	CShapeDlg::OnInitDialog();

	// TODO:  在此添加额外的初始化
	pRadio1 = (CButton*)GetDlgItem(IDC_RADIO1);
	pRadio2 = (CButton*)GetDlgItem(IDC_RADIO2);
	pRadio1->SetCheck(true);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}