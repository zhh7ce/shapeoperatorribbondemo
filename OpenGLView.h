// OpenGLView.h : COpenGLView 类的接口
//

#pragma once
#include "DemoDoc.h"


class COpenGLView : public CView {
protected: // 仅从序列化创建
	COpenGLView();
	DECLARE_DYNCREATE(COpenGLView)

	// 特性
public:
	CDemoDoc* GetDocument() const;

	// 操作
public:

	// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

	// 实现
public:
	virtual ~COpenGLView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()

private:
	HGLRC m_hRC;
	CClientDC* m_pDC;

public:
	CPoint m_point;
	COperator* m_operator;
	int m_operator_type;
	int m_operator_enable;


public:
	bool setPixelFormat();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void RenderOpenGL();
	void ScreenPosToWorldPos(CPoint point, float &objx, float &objy, float &objz);

	void DrawAxis();
	void SetViewPort(int x, int y, int cx, int cy);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnOpenglCube();
	afx_msg void OnOpenglCylinder();
};

#ifndef _DEBUG  // demo2View.cpp 中的调试版本
inline CDemoDoc* COpenGLView::GetDocument() const {
	return reinterpret_cast<CDemoDoc*>(m_pDocument);
}
#endif

