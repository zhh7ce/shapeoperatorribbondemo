#include "stdafx.h"
#include "Shape.h"

#define PI 3.1415927

// CShape

CShape::CShape() {
	len = 0;
	m_matrix = glm::mat4(1.0f);
	m_focused = -1;
	m_color = color_black;
}

CShape::~CShape() {
	delete[] m_points;
}

IMPLEMENT_SERIAL(CCube, CObject, 1)
IMPLEMENT_SERIAL(COperator, CObject, 1)
IMPLEMENT_SERIAL(CCylinder, CObject, 1)

void CShape::Serialize(CArchive & ar) {
	if (ar.IsStoring()) {	// storing code
		ar << len;
		for (int i = 0; i < len; i++) {
			ar << m_points[i].x;
			ar << m_points[i].y;
			ar << m_points[i].z;
		}
	} else {	// loading code
		ar >> len;
		m_points = new glm::vec4[len];
		GLfloat x, y, z;
		for (int i = 0; i < len; i++) {
			ar >> x;
			ar >> y;
			ar >> z;
			m_points[i] = glm::vec4(x, y, z, 1.0f);
		}
	}
}


void CShape::LoadPoints(glm::vec4* points) {
	memcpy(m_points, points, len * sizeof(glm::vec4));
	for (int i = 0; i < len; i++) {
		m_points[i] = m_matrix * m_points[i];
	}
}



CLine::CLine() {
	len = 2;
	m_points = new glm::vec4[len];
}


CLine::~CLine() {}

void CLine::Draw() {
	glColor4fv(m_color);
	glLineWidth(3);
	glBegin(GL_LINES);
	for (int i = 0; i < len; i++) {
		glVertex3f(m_points[i].x, m_points[i].y, m_points[i].z);
	}

	glEnd();
}



CCube::CCube() {
	len = 8;
	m_points = new glm::vec4[len];
}
CCube::~CCube() {}

void CCube::Draw() {
	glColor4fv(color_black);
	glLineWidth(1);
	glBegin(GL_LINES);
	for (int i = 0; i < 12; i++) {
		for (int j = 0; j < 2; j++) {
			glVertex3f(m_points[line_index[i][j]].x, m_points[line_index[i][j]].y, m_points[line_index[i][j]].z);
		}
	}
	glEnd();

	glColor4fv(color_little_blue);
	glBegin(GL_QUADS);
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 4; j++) {
			glVertex3f(m_points[quad_index[i][j]].x, m_points[quad_index[i][j]].y, m_points[quad_index[i][j]].z);
		}
	}
	glEnd();
}



COperator::COperator() {
	len = 7;
	m_visible = false;
	m_object_type = 0;
	m_move_shape = true;
	m_points = new glm::vec4[len];
	m_points[0] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	m_points[1] = glm::vec4(10.0f, 0.0f, 0.0f, 1.0f);
	m_points[2] = glm::vec4(0.0f, 10.0f, 0.0f, 1.0f);
	m_points[3] = glm::vec4(0.0f, 0.0f, 10.0f, 1.0f);
	m_points[4] = glm::vec4(0.0f, 5.0f, 5.0f, 1.0f);
	m_points[5] = glm::vec4(5.0f, 0.0f, 5.0f, 1.0f);
	m_points[6] = glm::vec4(5.0f, 5.0f, 0.0f, 1.0f);
}

COperator::~COperator() {}

void COperator::Draw() {
	if (m_visible == false) return;
	glColor4fv(color_black);
	glLineWidth(1);
	glBegin(GL_LINES);
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			glVertex3f(m_points[index[i][j]].x, m_points[index[i][j]].y, m_points[index[i][j]].z);
		}
	}
	glEnd();

	glBegin(GL_LINES);
	int a;
	int b;
	int list[3] = { 1, 2, 3 };
	GLfloat theta = 0.0f;
	GLfloat x, y, z;
	for (int i = 0; i < 3; i++) {
		a = list[i];
		b = list[(i + 1) % 3];
		theta = 0.0f;
		for (int j = 0; j < 20; j++) {
			x = m_points[0].x + 0.707f * (sin(theta) * (m_points[a] - m_points[0]).x + cos(theta) * (m_points[b] - m_points[0]).x);
			y = m_points[0].y + 0.707f * (sin(theta) * (m_points[a] - m_points[0]).y + cos(theta) * (m_points[b] - m_points[0]).y);
			z = m_points[0].z + 0.707f * (sin(theta) * (m_points[a] - m_points[0]).z + cos(theta) * (m_points[b] - m_points[0]).z);
			glVertex3f(x, y, z);
			theta += PI / 40;
		}
	}
	glEnd();

	glColor4fv(color_green);
	glPointSize(5);
	glBegin(GL_POINTS);
	for (int i = 0; i < 7; i++) {
		glVertex3f(m_points[i].x, m_points[i].y, m_points[i].z);
	}
	if (m_focused >= 0) {
		glColor4fv(color_red);
		glVertex3f(m_points[m_focused].x, m_points[m_focused].y, m_points[m_focused].z);
	}
	glEnd();
}

void COperator::Rotate(int axis, GLfloat degree) {
	if (m_visible == false) return;
	glm::vec3 translate_axis = m_points[0];
	glm::mat4 trans = glm::mat4(1.0f);
	trans = glm::translate(trans, translate_axis);

	glm::vec3 rotate_axis = m_points[axis] - m_points[0];
	trans = glm::rotate(trans, glm::radians(degree), rotate_axis);
	trans = glm::translate(trans, -translate_axis);

	for (int i = 0; i < m_object->len; i++) {
		m_object->m_points[i] = trans * m_object->m_points[i];
	}
	for (int i = 0; i < len; i++) {
		m_points[i] = trans * m_points[i];
	}

	m_object->m_matrix = trans * m_object->m_matrix;
	m_matrix = trans * m_matrix;

}

void COperator::Transalte(int axis, GLfloat distance) {
	if (m_visible == false) return;
	glm::vec3 translate_axis = m_points[axis] - m_points[0];
	translate_axis *= distance;

	glm::mat4 trans = glm::mat4(1.0f);
	trans = glm::translate(trans, translate_axis);

	for (int i = 0; i < m_object->len; i++) {
		m_object->m_points[i] = trans * m_object->m_points[i];
	}
	for (int i = 0; i < len; i++) {
		m_points[i] = trans * m_points[i];
	}
	m_object->m_matrix = trans * m_object->m_matrix;
	m_matrix = trans * m_matrix;
}

void COperator::MoveHandler(GLfloat x, GLfloat y, GLfloat z) {
	if (m_visible == false) return;
	for (int i = 0; i < m_object->len; i++) {
		if (abs(m_object->m_points[i].x - x) + abs(m_object->m_points[i].y - y) < 0.4f) {
			x = m_object->m_points[i].x;
			y = m_object->m_points[i].y;
			z = m_object->m_points[i].z;
		}
	}

	glm::vec4 end(x, y, z, 1.0f);
	glm::vec3 translate_axis = end - m_points[0];

	glm::mat4 trans = glm::mat4(1.0f);
	trans = glm::translate(trans, translate_axis);

	for (int i = 0; i < len; i++) {
		m_points[i] = trans * m_points[i];
	}
	m_matrix = trans * m_matrix;

}

void COperator::Serialize(CArchive& ar) {
	if (ar.IsStoring()) {	// storing code
		ar << len;
		for (int i = 0; i < len; i++) {
			ar << m_points[i].x;
			ar << m_points[i].y;
			ar << m_points[i].z;
		}
		ar << m_visible;
		ar << m_object_type;
		m_object->Serialize(ar);
	} else {	// loading code
		ar >> len;
		m_points = new glm::vec4[len];
		GLfloat x, y, z;
		for (int i = 0; i < len; i++) {
			ar >> x;
			ar >> y;
			ar >> z;
			m_points[i] = glm::vec4(x, y, z, 1.0f);
		}
		ar >> m_visible;
		ar >> m_object_type;
		switch (m_object_type) {
		case 1: m_object = new CCube(); break;
		case 2: m_object = new CCylinder(); break;
		default: break;
		}
		m_object->Serialize(ar);
	}
}


CCylinder::CCylinder() {
	len = 128;
	m_points = new glm::vec4[128];
}


CCylinder::~CCylinder() {}

void CCylinder::Draw() {
	/** Draw the tube */
	glColor4fv(color_little_blue);
	glBegin(GL_QUAD_STRIP);
	for (int i = 0; i < len; i++) {
		glVertex3f(m_points[i].x, m_points[i].y, m_points[i].z);
	}
	glEnd();

	/** Draw the circle on top of cylinder */
	glColor4fv(color_little_blue);
	glBegin(GL_POLYGON);
	for (int i = 0; i < len; i+=2) {
		glVertex3f(m_points[i].x, m_points[i].y, m_points[i].z);
	}
	glEnd();
	glBegin(GL_POLYGON);
	for (int i = len-2; i > 0; i -= 2) {
		glVertex3f(m_points[i].x, m_points[i].y, m_points[i].z);
	}
	glEnd();


	glColor4fv(color_black);
	glBegin(GL_LINES);
	for (int i = 0; i < len; i += 2) {
		glVertex3f(m_points[i].x, m_points[i].y, m_points[i].z);
	}
	glEnd();
	glBegin(GL_LINES);
	for (int i = 1; i < len; i += 2) {
		glVertex3f(m_points[i].x, m_points[i].y, m_points[i].z);
	}
	glEnd();
}