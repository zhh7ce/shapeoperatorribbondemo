#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


static 	GLfloat color_red[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
static 	GLfloat color_green[4] = { 0.0f, 1.0f, 0.0f, 1.0f };
static 	GLfloat color_blue[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
static 	GLfloat color_black[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
static 	GLfloat color_little_red[4] = { 1.0f, 0.0f, 0.0f, 0.2f };
static 	GLfloat color_little_green[4] = { 0.0f, 1.0f, 0.0f, 0.2f };
static 	GLfloat color_little_blue[4] = { 0.0f, 0.0f, 1.0f, 0.2f };
static 	GLfloat color_little_black[4] = { 0.0f, 0.0f, 0.0f, 0.2f };

static GLint line_index[12][2] = {
	{ 0, 1 },
	{ 2, 3 },
	{ 4, 5 },
	{ 6, 7 },
	{ 0, 2 },
	{ 1, 3 },
	{ 4, 6 },
	{ 5, 7 },
	{ 0, 4 },
	{ 1, 5 },
	{ 7, 3 },
	{ 2, 6 }
};

static GLint quad_index[6][4] = {
	{ 0, 2, 3, 1 },
	{ 0, 4, 6, 2 },
	{ 0, 1, 5, 4 },
	{ 4, 5, 7, 6 },
	{ 1, 3, 7, 5 },
	{ 2, 6, 7, 3 }
};

static GLint index[12][2] = {
	{ 0, 1 },
	{ 0, 2 },
	{ 0, 3 }
};

class CShape :
	public CObject {
public:
	CShape();
	~CShape();
	int len;
	glm::vec4* m_points;
	glm::mat4 m_matrix;
	
	int m_focused;

	GLfloat* m_color;
	virtual void Draw() = 0;
	virtual void Serialize(CArchive& ar);

	void LoadPoints(glm::vec4* points);
};



class CLine :
	public CShape {
public:
	CLine();
	~CLine();

	virtual void Draw();
};

class CCube :
	public CShape {
public:
	CCube();
	~CCube();
	DECLARE_SERIAL(CCube)

	virtual void Draw();
};

class COperator :
	public CShape {
public:
	COperator();
	~COperator();
	DECLARE_SERIAL(COperator)
	CShape* m_object;
	bool m_visible;
	int m_object_type;
	bool m_move_shape;

	virtual void Draw();
	void Rotate(int axis, GLfloat degree);
	void Transalte(int axis, GLfloat distance);
	void MoveHandler(GLfloat x, GLfloat y, GLfloat z);
	virtual void Serialize(CArchive& ar);
};

class CCylinder :
	public CShape {
public:
	CCylinder();
	~CCylinder();
	DECLARE_SERIAL(CCylinder)

	virtual void Draw();
};
