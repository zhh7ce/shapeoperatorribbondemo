// OpenGLView.cpp : COpenGLView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "RibbonControls.h"
#endif

#include "DemoDoc.h"
#include "OpenGLView.h"

#include "atlstr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// COpenGLView

IMPLEMENT_DYNCREATE(COpenGLView, CView)

BEGIN_MESSAGE_MAP(COpenGLView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	//ON_COMMAND(ID_OPENGL_CUBE, &COpenGLView::OnOpenglCube)
	//ON_COMMAND(ID_OPENGL_CYLINDER, &COpenGLView::OnOpenglCylinder)
END_MESSAGE_MAP()

static GLfloat xRot = 0.0f;
static GLfloat yRot = 0.0f;
static GLfloat zRot = 0.0f;

// COpenGLView 构造/析构

COpenGLView::COpenGLView() {
	// TODO: 在此处添加构造代码
	m_point = CPoint(0,0);
	m_operator = NULL;
	m_operator_type = 1;
	m_operator_enable = 1;
}

COpenGLView::~COpenGLView() {}

BOOL COpenGLView::PreCreateWindow(CREATESTRUCT& cs) {
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// COpenGLView 绘制

void COpenGLView::OnDraw(CDC* /*pDC*/) {
	CDemoDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
	RenderOpenGL();
}


// COpenGLView 打印

BOOL COpenGLView::OnPreparePrinting(CPrintInfo* pInfo) {
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void COpenGLView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/) {
	// TODO: 添加额外的打印前进行的初始化过程
}

void COpenGLView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/) {
	// TODO: 添加打印后进行的清理过程
}


// COpenGLView 诊断

#ifdef _DEBUG
void COpenGLView::AssertValid() const {
	CView::AssertValid();
}

void COpenGLView::Dump(CDumpContext& dc) const {
	CView::Dump(dc);
}

CDemoDoc* COpenGLView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDemoDoc)));
	return (CDemoDoc*)m_pDocument;
}
#endif //_DEBUG


// COpenGLView 消息处理程序


bool COpenGLView::setPixelFormat() {
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR), // 结构的大小  
		1, // 结构的版本  
		PFD_DRAW_TO_WINDOW | // 在窗口(而不是位图)中绘图  
		PFD_SUPPORT_OPENGL | // 支持在窗口中进行OpenGL调用  
		PFD_DOUBLEBUFFER, // 双缓冲模式  
		PFD_TYPE_RGBA, // RGBA颜色模式  
		32, // 需要32位颜色  
		0, 0, 0, 0, 0, 0, // 不用于选择模式  
		0, 0, // 不用于选择模式  
		0, 0, 0, 0, 0, // 不用于选择模式  
		16, // 深度缓冲区的大小  
		0, // 在此不使用  
		0, // 在此不使用  
		0, // 在此不使用  
		0, // 在此不使用  
		0, 0, 0 // 在此不使用  
	};
	// 选择一种与pfd所描述的最匹配的像素格式  
	// 为设备环境设置像素格式  
	int pixelformat;
	pixelformat = ChoosePixelFormat(m_pDC->GetSafeHdc(), &pfd);
	if (0 == pixelformat) return false;
	// 为设备环境设置像素格式  
	return SetPixelFormat(m_pDC->GetSafeHdc(), pixelformat, &pfd);
	return false;
}


int COpenGLView::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码
	m_pDC = new CClientDC(this);
	ASSERT(m_pDC != NULL);
	// 选择像素格式  
	if (!setPixelFormat()) return -1;
	// 创建渲染环境, 并使它成为当前渲染环境  
	m_hRC = wglCreateContext(m_pDC->GetSafeHdc());
	wglMakeCurrent(m_pDC->GetSafeHdc(), m_hRC);

	//GLfloat temp_matrix[8][4] = { 0,0,0,1, 0,10,0,1, 10,0,0,1, 10,10,0,1,
	//							  0,0,20,1, 0,10,20,1, 10,0,20,1, 10,10,20,1 };
	//glm::vec4 temp_vec[8];
	//for (int i = 0; i < 8; i++) {
	//	temp_vec[i] = glm::vec4(temp_matrix[i][0], temp_matrix[i][1], temp_matrix[i][2], temp_matrix[i][3]);
	//}
	//CShape* c = new CCube();
	//c->LoadPoints(temp_vec);
	//GetDocument()->m_shape_list.AddTail(c);

	//m_operator = new COperator();
	//m_operator->m_object = c;

	return 0;
}


void COpenGLView::OnDestroy() {
	CView::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(m_hRC);
	delete m_pDC;
}


void COpenGLView::OnInitialUpdate() {
	CView::OnInitialUpdate();

	// TODO: 在此添加专用代码和/或调用基类
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	 //glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glDisable(GL_CULL_FACE);
	glEnable(GL_CULL_FACE);
	//glutMotionFunc(MouseMotionEvent);

}


void COpenGLView::OnSize(UINT nType, int cx, int cy) {
	CView::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码

	RenderOpenGL();
}


BOOL COpenGLView::PreTranslateMessage(MSG* pMsg) {
	// TODO: 在此添加专用代码和/或调用基类
	CDemoDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return CView::PreTranslateMessage(pMsg);
	if (WM_KEYFIRST <= pMsg->message && pMsg->message <= WM_KEYLAST) {
		//if (m_operator == NULL) return CView::PreTranslateMessage(pMsg);
		switch (pMsg->wParam) {
		case VK_UP: xRot -= 5.0f; break;
		case VK_DOWN: xRot += 5.0f; break;
		case VK_LEFT: yRot -= 5.0f; break;
		case VK_RIGHT: yRot += 5.0f; break;
		default: break;
		}
		//m_operator->m_focused = m_operator_type;
	}
	
	RenderOpenGL();

	return CView::PreTranslateMessage(pMsg);
}


void COpenGLView::RenderOpenGL() {
	CDemoDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	CRect rc;
	GetClientRect(&rc);
	int cx = rc.Width();
	int cy = rc.Height();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	SetViewPort(0, 0, cx, cy);
	glPushMatrix();

	glRotatef(xRot, 1.0f, 0.0f, 0.0f);
	glRotatef(yRot, 0.0f, 1.0f, 0.0f);

	COperator* c;
	POSITION pos = pDoc->m_operator_list.GetHeadPosition();
	while (pos != NULL) {
		c = pDoc->m_operator_list.GetNext(pos);
		c->Draw();
		c->m_object->Draw();
	}

	//DrawAxis();
	glPopMatrix();
	SwapBuffers(wglGetCurrentDC());
	UpdateWindow();
}


void COpenGLView::ScreenPosToWorldPos(CPoint point, float &objx, float &objy, float &objz) {

	GLint viewport[4];
	GLdouble modelview[16] = { 0 };
	GLdouble projection[16];
	GLfloat winX, winY, winZ;
	GLdouble posX, posY, posZ;
	GLfloat mv[16];

	glGetFloatv(GL_MODELVIEW_MATRIX, mv);
	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	glGetIntegerv(GL_VIEWPORT, viewport);

	winX = (float)point.x;
	winY = (float)viewport[3] - (float)point.y;
	glReadPixels(point.x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

	gluUnProject(winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
	// float x = m_hRC.m_projectionMatrix[0][0];
	objx = posX;
	objy = posY;
	objz = posZ;
}




void COpenGLView::DrawAxis() {
	SetViewPort(0, 300, 200, 200);
	int AXES_LEN = 40;
	int AXES_RADIUS = 4;
	int ARROW_LEN = 8;
	int ARROW_RADIUS = 8;

	GLUquadricObj *objCylinder = gluNewQuadric();

	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	gluCylinder(objCylinder, AXES_RADIUS, AXES_RADIUS, AXES_LEN, 10, 5);            //z
	glTranslatef(0, 0, AXES_LEN);
	gluCylinder(objCylinder, ARROW_RADIUS, 0, ARROW_LEN, 10, 5);  //z arrow
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.0f, 1.0f, 0.0f);
	glRotatef(xRot+90, 1.0, 0.0, 0.0);
	gluCylinder(objCylinder, AXES_RADIUS, AXES_RADIUS, AXES_LEN, 10, 5);             //Y
	glTranslatef(0, 0, AXES_LEN);
	gluCylinder(objCylinder, ARROW_RADIUS, 0, ARROW_LEN, 10, 5);   //Y arrow
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.0f, 0.0f, 1.0f);
	glRotatef(yRot+90, 0.0, 1.0, 0.0);
	gluCylinder(objCylinder, AXES_RADIUS, AXES_RADIUS, AXES_LEN, 10, 5);              //X
	glTranslatef(0, 0, AXES_LEN);
	gluCylinder(objCylinder, ARROW_RADIUS, 0, ARROW_LEN, 10, 5);    //X arrow
	glPopMatrix();
	glFinish();
	glFlush();
}


void COpenGLView::SetViewPort(int x, int y, int cx, int cy) {
	GLfloat nRange = 20.0f;

	if (cy == 0)
		cy = 1;

	glViewport(x, y, cx, cy);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (cx <= cy)
		glOrtho(-nRange, nRange, -nRange*cy / cx, nRange*cy / cx, -nRange, nRange);
	else
		glOrtho(-nRange*cx / cy, nRange*cx / cy, -nRange, nRange, -nRange, nRange);

	glPushMatrix();
	glRotatef(xRot, 1.0f, 0.0f, 0.0f);
	glRotatef(yRot, 0.0f, 1.0f, 0.0f);
}


void COpenGLView::OnMouseMove(UINT nFlags, CPoint point) {
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (nFlags != MK_LBUTTON) return;
	if (!m_operator) return;
	if (m_operator_type == 0) {
		GLfloat x, y, z;
		ScreenPosToWorldPos(point, x, y, z);
		m_operator->MoveHandler(x, y, z);
	} else if (m_operator_type < 4) {
		if (point.x > m_point.x || point.y > m_point.y) {
			m_operator->Transalte(m_operator_type, 0.01f);
		} else {
			m_operator->Transalte(m_operator_type, -0.01f);
		}
	} else {
		if (point.x > m_point.x || point.y > m_point.y) {
			m_operator->Rotate(m_operator_type-3, 1.0f);
		} else {
			m_operator->Rotate(m_operator_type-3, -1.0f);
		}
	}
	m_point.x = point.x;
	m_point.y = point.y;
	
	RenderOpenGL();

	CView::OnMouseMove(nFlags, point);
}


void COpenGLView::OnLButtonDown(UINT nFlags, CPoint point) {
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CDemoDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	GLfloat objx, objy, objz;
	ScreenPosToWorldPos(point, objx, objy, objz);

	POSITION pos = pDoc->m_operator_list.GetHeadPosition();
	COperator *o;
	GLfloat d_min = 1.0f;
	GLfloat d;

	while (pos != NULL) {
		o = pDoc->m_operator_list.GetNext(pos);
		if (o->m_visible) {
			if (o->m_move_shape) {
				for (int i = 1; i < 7; i++) {
					d = abs(o->m_points[i].x - objx);
					d += abs(o->m_points[i].y - objy);
					//d += abs(m_operator->m_points[i].z - objz);
					if (d < d_min) {
						d_min = d;
						m_operator = o;
						m_operator_type = i;
						m_operator->m_focused = i;
					}
				}
			} else {
				m_operator = o;
				m_operator_type = 0;
				m_operator->m_focused = 0;
			}

			
		}
	}
	RenderOpenGL();
	CView::OnLButtonDown(nFlags, point);
}


//void COpenGLView::OnOpenglCube() {
//	// TODO: 在此添加命令处理程序代码
//	m_shape_dlg = new CCubeDlg();
//	m_shape_dlg->Create(IDD_CUBEDLG, this);
//	m_shape_dlg->m_pDoc = GetDocument();
//	m_shape_dlg->CenterWindow();
//	m_shape_dlg->ShowWindow(SW_NORMAL);
//
//}
//
//
//void COpenGLView::OnOpenglCylinder() {
//	// TODO: 在此添加命令处理程序代码
//	m_shape_dlg = new CCylinderDlg();
//	m_shape_dlg->Create(IDD_CYLINDERDLG, this);
//	m_shape_dlg->m_pDoc = GetDocument();
//	m_shape_dlg->CenterWindow();
//	m_shape_dlg->ShowWindow(SW_NORMAL);
//}
