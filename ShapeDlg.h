#pragma once
#include "DemoDoc.h"
#include "afxdialogex.h"

class CShapeDlg : public CDialogEx {
	DECLARE_DYNAMIC(CShapeDlg)

public:
	COperator* m_o;
	CDemoDoc* m_pDoc;
	CShapeDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CShapeDlg();
	virtual void OnCancel();
	virtual void PostNcDestroy();
	void LoadPtr(COperator* o, CDemoDoc* pDoc);
};


// CCubeDlg 对话框

class CCubeDlg : public CShapeDlg
{
	DECLARE_DYNAMIC(CCubeDlg)

public:
	CCubeDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCubeDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CUBEDLG };
#endif

	DECLARE_MESSAGE_MAP()
public:
	
	afx_msg void OnBnClickedOk();
};
#pragma once


// CCylinderDlg 对话框

class CCylinderDlg : public CShapeDlg
{
	DECLARE_DYNAMIC(CCylinderDlg)

public:
	CCylinderDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCylinderDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CYLINDERDLG };
#endif

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};



class COperatorDlg : public CShapeDlg {
	DECLARE_DYNAMIC(COperatorDlg)

public:
	COperatorDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~COperatorDlg();

	CButton* pRadio1;
	CButton* pRadio2;

	// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_OPERATORDLG };
#endif

	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	virtual BOOL OnInitDialog();
};
