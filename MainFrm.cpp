// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "RibbonControls.h"
#include "OpenGLView.h"
#include "ShapeTreeView.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	

	ON_COMMAND(ID_OPENGL_CUBE, &CMainFrame::OnOpenglCube)
	ON_COMMAND(ID_OPENGL_CYLINDER, &CMainFrame::OnOpenglCylinder)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	XTPPaintManager()->SetTheme(xtpThemeRibbon);

	if (!CreateStatusBar())
		return -1;

	if (!InitCommandBars())
		return -1;

	CXTPCommandBars* pCommandBars = GetCommandBars();
	m_wndStatusBar.SetCommandBars(pCommandBars);

	if (!CreateRibbonBar())
	{
		TRACE0("Failed to create ribbon\n");
		return -1;
	}

	CXTPToolTipContext* pToolTipContext = GetCommandBars()->GetToolTipContext();
	pToolTipContext->SetStyle(xtpToolTipResource);
	pToolTipContext->ShowTitleAndDescription();
	pToolTipContext->ShowImage(TRUE, 0);
	pToolTipContext->SetMargin(CRect(2, 2, 2, 2));
	pToolTipContext->SetMaxTipWidth(180);
	pToolTipContext->SetFont(pCommandBars->GetPaintManager()->GetIconFont());
	pToolTipContext->SetDelayTime(TTDT_INITIAL, 900);

	pCommandBars->GetCommandBarsOptions()->bShowKeyboardTips = TRUE;


	return 0;
}

BOOL CMainFrame::CreateStatusBar()
{
	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return FALSE;      // fail to create
	}

	m_wndStatusBar.AddIndicator(0);
	m_wndStatusBar.AddIndicator(ID_INDICATOR_CAPS);
	m_wndStatusBar.AddIndicator(ID_INDICATOR_NUM);
	m_wndStatusBar.AddIndicator(ID_INDICATOR_SCRL);

	return TRUE;
}

BOOL CMainFrame::CreateRibbonBar()
{
	CXTPCommandBars* pCommandBars = GetCommandBars();

	CMenu menu;
	menu.Attach(::GetMenu(m_hWnd));
	SetMenu(NULL);

	CXTPRibbonBar* pRibbonBar = (CXTPRibbonBar*)pCommandBars->Add(_T("The Ribbon"), xtpBarTop, RUNTIME_CLASS(CXTPRibbonBar));
	if (!pRibbonBar)
	{
		return FALSE;
	}

	pRibbonBar->EnableDocking(0);

	CXTPControlPopup* pControlFile = (CXTPControlPopup*)pRibbonBar->AddSystemButton(ID_MENU_FILE);
	pControlFile->SetCommandBar(menu.GetSubMenu(0));
	pControlFile->GetCommandBar()->SetIconSize(CSize(32, 32));
	pCommandBars->GetImageManager()->SetIcons(ID_MENU_FILE);
	pControlFile->SetCaption(_T("&File"));
	
	pControlFile->SetIconId(IDB_GEAR);
	UINT uCommand = {IDB_GEAR};
	pCommandBars->GetImageManager()->SetIcons(IDB_GEAR, &uCommand, 1, CSize(0, 0), xtpImageNormal);
	
	CXTPRibbonTab* pTabHome = pRibbonBar->AddTab(ID_TAB_BUTTONS);

	// Large Buttons
	if (pTabHome)
	{
		CXTPControl* pControl;

		CXTPRibbonGroup* pGroup = pTabHome->AddGroup(ID_GROUP_LARGEBUTTONS);
		pGroup->ShowOptionButton();

		pControl = pGroup->Add(xtpControlButton, ID_FILE_OPEN);
		pControl->SetStyle(xtpButtonIconAndCaptionBelow);

		pControl = pGroup->Add(xtpControlButton, ID_FILE_SAVE);
		pControl->SetStyle(xtpButtonIconAndCaptionBelow);

		//UINT nIDs[] = {ID_BUTTON_LARGESIMPLEBUTTON, ID_BUTTON_LARGEPOPUPBUTTON, ID_BUTTON_LARGESPLITPOPUPBUTTON, ID_BUTTON_LARGETOGGLEBUTTON};
		//pCommandBars->GetImageManager()->SetIcons(ID_GROUP_LARGEBUTTONS, nIDs, 4, CSize(32, 32), xtpImageNormal);
	}

	//if (pTabHome)
	//{
	//	CXTPRibbonGroup* pGroup = pTabHome->AddGroup(ID_GROUP_GROUP);
	//	pGroup->SetControlsGrouping(TRUE);
	//	pGroup->SetControlsCentering(TRUE);

	//	pGroup->LoadToolBar(IDR_MAINFRAME);
	//}

	CXTPRibbonTab* pTabOpenGL = pRibbonBar->AddTab(ID_TAB_OPENGL);

	// Standard OpenGL
	if (pTabOpenGL)
	{
		CXTPControl* pControl;

		CXTPRibbonGroup* pGroup = pTabOpenGL->AddGroup(ID_GROUP_OPENGL);
		pGroup->ShowOptionButton();

		pControl = pGroup->Add(xtpControlButton, ID_OPENGL_CUBE);
		pControl->SetIconId(IDB_GEAR);
		pControl->SetWidth(64);
		pControl->SetStyle(xtpButtonIconAndCaptionBelow);

		pControl = pGroup->Add(xtpControlButton, ID_OPENGL_CYLINDER);
		pControl->SetIconId(IDB_GEAR);
		pControl->SetWidth(64);
		pControl->SetStyle(xtpButtonIconAndCaptionBelow);
	}

	// Options
	//{
	//	CXTPControlPopup* pControlOptions = (CXTPControlPopup*)pRibbonBar->GetControls()->Add(xtpControlPopup, -1);
	//	pControlOptions->SetFlags(xtpFlagRightAlign);
	//	CMenu mnuOptions;
	//	mnuOptions.LoadMenu(IDR_MENU_OPTIONS);
	//	pControlOptions->SetCommandBar(mnuOptions.GetSubMenu(0));
	//	pControlOptions->SetCaption(_T("Options"));
	//	
	//	
	//	CXTPControl* pControlAbout = pRibbonBar->GetControls()->Add(xtpControlButton, ID_APP_ABOUT);
	//	pControlAbout->SetFlags(xtpFlagRightAlign);

	//	pCommandBars->GetImageManager()->SetIcons(IDR_MAINFRAME);	

	//}

	// Quick Access
	{

		pRibbonBar->GetQuickAccessControls()->Add(xtpControlButton, ID_FILE_NEW);
		pRibbonBar->GetQuickAccessControls()->Add(xtpControlButton, ID_FILE_OPEN)->SetHideFlag(xtpHideCustomize, TRUE);
		pRibbonBar->GetQuickAccessControls()->Add(xtpControlButton, ID_FILE_SAVE);
		
		pRibbonBar->GetQuickAccessControls()->Add(xtpControlButton, ID_FILE_PRINT);
		pRibbonBar->GetQuickAccessControls()->CreateOriginalControls();

	}

	pRibbonBar->SetCloseable(FALSE);
	pRibbonBar->EnableFrameTheme();

	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.cx = 730;
	cs.cy = 600;

	cs.lpszClass = _T("XTPMainFrame");
	CXTPDrawHelpers::RegisterWndClass(AfxGetInstanceHandle(), cs.lpszClass, 
		CS_DBLCLKS, AfxGetApp()->LoadIcon(IDR_MAINFRAME));

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;


	return TRUE;
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// TODO: 在此添加专用代码和/或调用基类
		CRect rc;

	// 获取框架窗口客户区的CRect对象   
	GetClientRect(&rc);

	// 创建静态分割窗口，两行一列   
	if (!m_wndSplitter.CreateStatic(this, 1, 2))
		return FALSE;

	// 创建上面窗格中的视图   
	if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CShapeTreeView), CSize(rc.Width() / 8, rc.Height()), pContext))
		return FALSE;

	// 创建下面窗格中的视图   
	if (!m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(COpenGLView), CSize(rc.Width() / 8 * 7, rc.Height()), pContext))
		return FALSE;

	return TRUE;

	return CXTPFrameWnd::OnCreateClient(lpcs, pContext);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnOpenglCube()
{
	// TODO: 在此添加命令处理程序代码
	m_shape_dlg = new CCubeDlg();
	m_shape_dlg->Create(IDD_CUBEDLG, this);
	m_shape_dlg->m_pDoc = (CDemoDoc*) GetActiveDocument();
	m_shape_dlg->CenterWindow();
	m_shape_dlg->ShowWindow(SW_NORMAL);
}


void CMainFrame::OnOpenglCylinder()
{
	// TODO: 在此添加命令处理程序代码
	m_shape_dlg = new CCylinderDlg();
	m_shape_dlg->Create(IDD_CYLINDERDLG, this);
	m_shape_dlg->m_pDoc = (CDemoDoc*) GetActiveDocument();
	m_shape_dlg->CenterWindow();
	m_shape_dlg->ShowWindow(SW_NORMAL);
}
